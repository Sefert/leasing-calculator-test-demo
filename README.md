##### LeasingCalculatorDemo  
Using Robot Framework and python  
Testing some testcases in BDD and DDT(additional info in attributes file)  
by Marko Linde  
  
Needed: pip, python 3.6+, chromedriver.exe, geckodriver.exe - setup in your environment with paths  
Extra needed libraries location in Helpers folder  
  
Tests location: TestCases  
  
Launching testSuite helper genLeaseCalcReport.bat  
Linux users can use the command from inside bat  
 

