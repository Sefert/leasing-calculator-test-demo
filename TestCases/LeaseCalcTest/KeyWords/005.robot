*** Settings ***
Library  SeleniumLibrary
Library  Collections
Library  ../../../ModuleLibrary/VersionSetting.py
Library  OperatingSystem

*** Variables ***
${Browser}
${error_list}
${Version}
${error_messages}

*** Keywords ***
API version
    [Arguments]  ${Version}
    set test variable  ${Version}

Browser
    [Arguments]  ${Browser}
    #TODO: figure out why cant access directly
    ${url}=  url attribute  ${Version}
    open browser  ${url}  ${Browser}

    set test variable  ${Browser}

    maximize browser window
    click element  class:accept-selected


Wrong Monetary Value Is Entered
    [Arguments]  ${error_message_locator}
    ${iframe}=  iframe attribute  ${version}
    ${vechicle_attribute}=  vehicle price attribute  ${version}
    ${reposition}=  interest rate attribute  ${version}

    select frame  ${iframe}

    #TODO: move to module and create better data collection
    ${data_set}=  create list  0  <script></script>
    ${str_len}=  get length  ${data_set}

    ${error_list}=  create list
    ${error_messages}=  create list

    #Evaluate data(input) collection
    :FOR  ${elem}  IN RANGE  0  ${str_len}
    \  input text  ${vechicle_attribute}  ${data_set}[${elem}]
    \  press key  ${vechicle_attribute}  \\13
    \  sleep  1 seconds
    #TODO: not working as expected - change to sleep
    \  #wait until created  ${error_message_locator}
    \  ${attribute}=  Get Element Attribute  ${error_message_locator}  class
    \  append to list  ${error_list}  ${attribute}
    \  ${message}=  Collect Element If Present  ${error_message_locator}
    \  append to list  ${error_messages}  ${message}

    #Evaluate empty input
    clear element text  ${vechicle_attribute}
    press key  ${vechicle_attribute}  \\13

    ${message}=  Collect Element If Present  ${error_message_locator}
    append to list  ${error_messages}  ${message}
    ${attribute}=  Get Element Attribute  ${error_message_locator}  class
    append to list  ${error_list}  ${attribute}

    set test variable  ${error_list}
    set test variable  ${error_messages}

Input Should Turn Red
    [Arguments]  ${error_class_attribute}
    log  ${error_list}
    ${str_len}=  get length  ${error_list}

    :FOR  ${elem}  IN RANGE  0  ${str_len}
    \  run keyword and continue on failure  should be equal as strings  ${error_list}[${elem}]  ${error_class_attribute}

Correct Error Message Is Shown
    [Arguments]  ${expected_error_messages}
    log  ${error_messages}
    ${str_len}=  get length  ${error_list}

    :FOR  ${elem}  IN RANGE  0  ${str_len}
    \  run keyword and continue on failure  should be equal as strings  ${error_messages}[${elem}]  ${expected_error_messages}[${elem}]

Collect Element If Present
    [Arguments]  ${attribute}
    ${count}=  get element count  ${attribute}
    ${message}=  Run Keyword And Return If   ${count} > 0  get text  ${attribute}
    [Return]  ${message}