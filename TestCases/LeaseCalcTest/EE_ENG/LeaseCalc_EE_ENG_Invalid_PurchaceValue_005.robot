*** Settings ***
Library  SeleniumLibrary
Resource  ../KeyWords/005.robot

Test Setup  Load Test Indentifiers
Test Teardown  close browser

*** Variables ***
${expected_error__messages}
${error_present_locator}
${error_message_locator}
${api_version}
${browser}


*** Test Cases ***
LeaseCalc_EE_ENG_PurchaceValue_UnderRequierment_005
    [Tags]  UiUnit  Medium  LowPriority
    Given API version  ${api_version}
    And browser  ${browser}
    When wrong monetary value is entered  ${error_message_locator}
    Then input should turn red  ${error_present_locator}



*** Keywords ***
Load Test Indentifiers
    ${api_version}=  set variable  ee
    ${browser}=  set variable  Firefox
    ${error_message_locator}=  set variable  xpath://div[@class='col1']//tr[3]
    ${error_present_locator}=  set variable  error
    ${expected_error__messages}=  create list

    set test variable  ${api_version}
    set test variable  ${browser}
    set test variable  ${error_message_locator}
    set test variable  ${error_present_locator}
    set test variable  ${expected_error__messages}