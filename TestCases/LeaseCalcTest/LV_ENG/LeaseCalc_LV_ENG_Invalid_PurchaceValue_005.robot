*** Settings ***
Library  SeleniumLibrary
Resource  ../KeyWords/005.robot

Test Setup  Load Test Indentifiers
Test Teardown  close browser

*** Variables ***
${expected_error__messages}
${error_present_locator}
${error_message_locator}
${api_version}
${browser}


*** Test Cases ***
LeaseCalc_LV_ENG_PurchaceValue_UnderRequierment_005
    [Tags]  UiUnit  Medium  LowPriority
    Given API version  ${api_version}
    And browser  ${browser}
    When wrong monetary value is entered  ${error_message_locator}
    Then input should turn red  ${error_present_locator}
    And correct error message is shown  ${expected_error__messages}

*** Keywords ***
Load Test Indentifiers
    ${api_version}=  set variable  lv
    ${browser}=  set variable  Chrome
    ${error_message_locator}=  set variable  id:f-summa-error
    ${error_present_locator}=  set variable  error error-text
    ${expected_error__messages}=  create list  For amount up to 9 000 EUR we offer consumer loan
    ...  Wrong data format. The number must be greater than 0!  Wrong data format

    set test variable  ${api_version}
    set test variable  ${browser}
    set test variable  ${error_message_locator}
    set test variable  ${error_present_locator}
    set test variable  ${expected_error__messages}