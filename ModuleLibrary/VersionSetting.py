import json


# TODO make more generic if possible
class VersionAttribute:
    def __init__(self, version, iframe, url, vehicle_price, down_payment, interest_rate, residual_value,
                 contract_period, down_payment_type, compare, schedule):
        self.version = version,
        self.url = url
        self.iframe = iframe
        self.vehicle_price = vehicle_price,
        self.down_payment = down_payment,
        self.interest_rate = interest_rate,
        self.residual_value = residual_value,
        self.contract_period = contract_period,
        self.down_payment_type = down_payment_type,
        self.compare = compare,
        self.schedule = schedule

    @classmethod
    def load_json(cls, json_string):
        json_dict = json.loads(json_string)
        return cls(**json_dict)

    @classmethod
    def load_empty(cls):
        return cls

    # TODO: Make full represent
    def __repr__(self):
        return f'{self.version, self.url, self.vehicle_price, self.down_payment, self.interest_rate}'


version_attribute_list = []
with open('./Resources/attribute.json', 'r') as json_file:
    json_data = json.loads(json_file.read())
    for elem in json_data:
        version_attribute_list.append(VersionAttribute(**elem))


def clean_json_value(string):
    for letter in '(),\' ':
        string = string.replace(letter, '')
    return string


def url_attribute(version):
    if version == 'ee':
        return clean_json_value(str(version_attribute_list[0].url))
    elif version == 'lv':
        return clean_json_value(str(version_attribute_list[1].url))
    elif version == 'lt':
        return clean_json_value(str(version_attribute_list[2].url))


def vehicle_price_attribute(version):
    if version == 'ee':
        return clean_json_value(str(version_attribute_list[0].vehicle_price))
    elif version == 'lv':
        return clean_json_value(str(version_attribute_list[1].vehicle_price))
    elif version == 'lt':
        return clean_json_value(str(version_attribute_list[2].vehicle_price))


def iframe_attribute(version):
    if version == 'ee':
        return clean_json_value(str(version_attribute_list[0].iframe))
    elif version == 'lv':
        return clean_json_value(str(version_attribute_list[1].iframe))
    elif version == 'lt':
        return clean_json_value(str(version_attribute_list[2].iframe))


def interest_rate_attribute(version):
    if version == 'ee':
        return clean_json_value(str(version_attribute_list[0].interest_rate))
    elif version == 'lv':
        return clean_json_value(str(version_attribute_list[1].interest_rate))
    elif version == 'lt':
        return clean_json_value(str(version_attribute_list[2].interest_rate))

# TODO: turn all attribute methods to version related
"""
def down_payment_attribute():
    return clean_json_value(str(version_module.down_payment))


def residual_value_attribute():
    return clean_json_value(str(version_module.residual_value))


def contract_period_attribute():
    return clean_json_value(str(version_module.contract_period))


def down_payment_type_attribute():
    return clean_json_value(str(version_module.down_payment_type))


def compare_button_attribute():
    return clean_json_value(str(version_module.compare))


def schedule_button_attribute():
    return clean_json_value(str(version_module.schedule))
"""